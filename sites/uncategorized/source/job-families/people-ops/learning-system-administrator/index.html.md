---
layout: job_family_page
title: Learning System Administrator
---

## Levels

### Associate Learning System Administrator

The Associate Learning System Administrator reports into the [Learning & Development Partner](/job-families/people-ops/learning-development/#learning-and-development-partner). 

#### Associate Learning System Administrator Job Grade

The Associate Learning System Administrator is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Associate Learning System Administrator Responsibilities

- Monitor and resolve learner issues on GitLab Learn using Zendesk, Slack, the GitLab Community Forum, and email. This role will be the Directly Responsible Individual (DRI) for all free user community support, including GitLab Education program members and wider community members.
- Enable GitLab team members and wider community members to contribute learning content to GitLab Learn
- Maintain security requirements, role permissions, group segmentation, and public versus private content in the platform, including management of user permissions and roles 
- Build and implement a scalable system to moderate user and team members contributed content and activity using the [GitLab Community Code of Conduct](/community/contribute/code-of-conduct/)
- Project management and coordination across all functional audiences using the LXP including Learning and Development, Field Enablement, Professional Services, Channel and Alliance Partners, and Community Relations
- Collaborate with the Learning and Development team, People Business Partners, and managers to create key learning reports
- Complete data maintenance with EdCast API and internal integration with Sisense dashboards 
- Track and monitor all compliance related courses (anti-harassment, corruption, bribery, etc) and follow up with users in a timely manner
- Track and monitor LinkedIn Learning usage, data, metrics, and reporting
- Iteratively document tasks, workflows, and processes in the GitLab handbook, focusing on the inclusion of high quality video to supplement text-based contributions
- Meet all milestones and final deliverables by deadlines; when delays do occur, work with the team to estimate, monitor, adjust, and proactively communicate revised deadlines as needed
- Suggest and implement improvements to LXP administration workflows

#### Associate Learning System Administrator Requirements

- Experience with learning management systems and/or learning experience platforms is preferred, specifically EdCast learning systems
- You share our [values](/handbook/values), and work in accordance with those values
- Excellent organizational, task, and time management skills, with an emphasis on detail and follow up, plus the ability to prioritize tasks and requests from multiple stakeholders and audiences
- Demonstrated ability to communicate and collaborate with individuals at all levels of the organization with excellent customer service skills
- Ability to work in a fast-paced environment with demonstrated ability to coordinate multiple projects/initiatives simultaneously while meeting deadlines and business objectives 
- Exhibit strong data skills and ability to utilize Google Sheets
- Ability to use, or willingness to learn, GitLab
- Bachelor’s degree in Training Education or Communication is preferred
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
- Clear, strong oral and written communication skills and can advocate for yourself and your role as a manager of one
- Experience working with various virtual meeting management tools such as Zoom is a plus
- 1-2 years related work experience (or equivalent combination of transferable experience and education)
- Experience in successfully prioritizing, managing multiple projects, and working in a team environment
- Ability to learn new systems quickly, and preferably have experience using GitLab, Zendesk, Discourse, EdCast, or any learning management or learning experience platforms

### Intermediate Learning System Administrator

The Intermediate Learning System Administrator reports into the [Learning & Development Partner](/job-families/people-ops/learning-development/#learning-and-development-partner). 

#### Intermediate Learning System Administrator Job Grade

The Intermediate Learning System Administrator is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Intermediate Learning System Administrator Responsibilities

- Extends the Associate Learning System Administrator responsibilities 
- Collaborate with People Ops team to build efficient and scalable workflows for learning administration and reporting
- Closely partner and collaborate with other members of the Learning & Development team to continuously improve existing learning programs based on the latest research in adult education
- Collaborate with the Brand and Technical Marketing teams to keep GitLab Learn platform design up to date, feature new learning programs, and follow GitLab branding guidelines and standards
- Interface between GitLab and learning vendors (EdCast, LinkedIn Learning, etc)

#### Intermediate Learning System Administrator Requirements

- Extends the Associate Learning System Administrator requirements 
- 3+ years related work experience (or equivalent combination of transferable experience and education)
- Proven experience designing and developing scalable, engaging, online training is a must
- Experience working with a variety of Learning Management Systems and Learning Experience Platforms such as EdCast

### Senior Learning System Administrator

The Senior Learning System Administrator reports into the [Learning & Development Partner](/job-families/people-ops/learning-development/#learning-and-development-partner). 

#### Senior Learning System Administrator Job Grade

The Senior Learning System Administrator Designer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Learning System Administrator Responsibilities

- Be an advocate for handbook first learning material and think creatively about how the handbook can be integrated into the LXP
- Collaborate with Learning Evangelists and GitLab managers to enable team members to reach their professional development goals using the GitLab Learn platform
- Design and implement initiatives to increase learner engagement and results with GitLab Learn and LinkedIn Learning
- Scale GitLab Learn by partnering with the organization to bring new and innovative learning solutions to team members and the community
- Build custom reporting capabilities to track learner engagement and completion

#### Senior Learning System Administrator Requirements

- Extends the Learning System Administrator (Intermediate) requirements 
- 5+ years related work experience (or equivalent combination of transferable experience and education)

## Performance Indicators 

- [Engagement Survey Growth and Development Score](https://about.gitlab.com/handbook/people-group/learning-and-development/#performance-indicators)
- [Exit Survey Data < 5 % of team members exiting GitLab related to L&D](https://about.gitlab.com/handbook/people-group/learning-and-development/#performance-indicators)

## Career Ladder

The next step for the Learning Systems Administrator is not yet defined.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.

- Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a 30 minute interview with our Learning & Development Manager. 
- Next, candidates will be asked to submit a short writing sample to answer a question that the Learning and Development team often sees from customers and community members.
- Next, the candidate will be invited to interview with a member of the Learning & Development team, a team member from our Sales Enablement, Professional Services, and/or Community Relations team. 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
